# Building the docs

## via Makefile

`make docs` from root of project

NOTE: do not commit generated files

## via ReadTheDocs

https://readthedocs.org/projects/duplicity-py3/builds/

NOTE: should run automatically when repo changes

# setup

https://gitlab.com/AlphaPrime/duplicity/-/blob/main/docs/conf.py
https://gitlab.com/AlphaPrime/duplicity/-/blob/main/readthedocs.yaml
